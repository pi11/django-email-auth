# Simple Django email-auth app

# This app provide email authentication and user options app.

Uses:

* install django-email-auth app - python setup.py install
* add 'emailuser' app to INSTALLED_APP
* add 'emailuser.auth.EmailAuthBackend' to AUTHENTICATION_BACKENDS
* run python manage.py syncdb

### Add views for login/register. Example:

    from django.contrib.auth.models import User
    from emailuser.models import EmailUser, EmailUserForm, manual_create_email_user

	def login_view(request):
		next_url = request.GET.get('next', '')
		sended = False
		if request.method == 'POST':
			form = EmailUserForm(request.POST) 
			if form.is_valid():  # All validation rules pass
				email = form.cleaned_data['email']
				try:
					user = User.objects.get(email=email)
				except User.DoesNotExist: # create user if not exists
					user = User.objects.create_user(
						email=form.cleaned_data['email'],
						username=form.cleaned_data[
							'email'],
						password=SOME_PASSWORD) # Set some password, generate new one for example
					user.save()
				try:
					token = user.emailuser.generate_token()
				except:
					manual_create_email_user(user)
					token = user.emailuser.generate_token()
				# try:
				if settings.DEBUG:
					print("Token - %s" % token)
				message = ('\nДля входа на сайт пройдите по ссылке:'
						   '%s/user/auth/%s/%s/?next=%s \n'
						   'Если у Вас возникли проблемы со входом '
						   'напишите письмо на %s' % (settings.SITE_URL, user.pk,
													  token, next_url,
													  settings.SERVER_EMAIL))

				# send email message here
				sended = True
		else:
			form = EmailUserForm()
		return render(request, "user/login.html", {'form': form, 'sended': sended})

    # auth example
	def auth(request, user_id, token):
		next_url = request.GET.get('next', None)
		# we can redirect user after login
		get_user = get_object_or_404(User, pk=user_id)
		user = authenticate(email=get_user.email, token=token)
		if user is not None:
			if user.is_active:
				login(request, user)
		return render(request, "user/auth.html", {"user": user, "next":next_url})



###### Example login.html:

    {% extends "base.html" %}
    {% block content %}
    {% if user.is_authenticated %}
      Welcome {{ user }}!
    {% else %}
	{% if sended %}
	   We just sent you link for login to our site.
	{% else %}
            <form action="" method="post">{% csrf_token %}
               {{ form.as_p }}
            <input type="submit" value="Login/Register" />
           </form>
        {% endif %}
    {% endif %}
    {% endblock %}

### Example authentication view:

    def auth(request, user_id, token):
        get_user = get_object_or_404(User, pk=user_id)
        user = authenticate(email=get_user.email, token=token)
        if user is not None:
            if user.is_active:
                login(request, user)
        return render(request, "user/auth.html", {"user": user})

###### Next in your code you can set:

    def some_view(request):
        user = request.user
        user.emailuser.set_option('some_option', 'some_value') # set option

###### And get user options:

    def another_view(request):
        value = user.emailuser.get_option('some_option') # get option, value = 'some_value'
