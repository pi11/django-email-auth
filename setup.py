from setuptools import setup, find_packages

setup(name='emailuser',
      version='0.3.8',
      description='Simple email, telegram token auth django app',
      url='https://bitbucket.org/pi11/django-email-auth',
      #packages=("emailuser",),
      packages=find_packages(),
      package_data={'emailuser': ['templates/user/*', 'emailuser/migrations/*']},
      include_package_data=True,
      )
