# -*- coding: utf-8 -*-
"""Django auth backend for EMailUser"""

import traceback
import logging

from emailuser.models import (TelegramUser, EmailUser, manual_create_telegram_user,
                              manual_create_email_user, TokenException)

from django.contrib.auth.models import User
from django.conf import settings

logger = logging.getLogger(__name__)

class EmailAuthBackend:
    """
    Authenticate user with token sended to email
    """

    def authenticate(self, request, email=False, tg_user_id=False, token=False):
        """Authenticate user"""
        if settings.DEBUG:
            print("Email - %s, Tg_user: %s. token - %s" % (email, tg_user_id, token))
        if email:
            try:
                user = User.objects.get(email=email)
            except User.DoesNotExist:
                if settings.DEBUG:
                    print("User does not exists")
                return None
            try:
                EmailUser.objects.get(user=user)
            except EmailUser.DoesNotExist:
                if settings.DEBUG:
                    print("Email User does not exists, try to create..")
                manual_create_email_user(user)
            auth = False
            try:
                auth = user.emailuser.do_auth(token)
            except TokenException:
                if settings.DEBUG:
                    print("Token Exception")
                    print(traceback.format_exc())
                auth = False

            if auth:
                if settings.DEBUG:
                    print("User auth OK")
                return user
            
        if tg_user_id:
            try:
                tg_user = TelegramUser.objects.get(tg_user_id=tg_user_id)
            except TelegramUser.DoesNotExist:
                if settings.DEBUG:
                    print("Telegram User does not exists, sorry")
                    return None
            auth = False
            try:
                auth = tg_user.do_auth(token)
            except TokenException:
                if settings.DEBUG:
                    print("Token Exception")
                    print(traceback.format_exc())
                auth = False

            if auth:
                if settings.DEBUG:
                    print("Telegram user auth OK")
                return tg_user.user
            
        return None

    def get_user(self, user_id):
        """Get user, Django require this method"""
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
