# -*- coding: utf-8 -*-
"""EmailUser models"""

import random
from random import choice
import string
from datetime import datetime, timedelta

import logging
import hashlib
import pytz

from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings

logger = logging.getLogger(__name__)


class TokenException(Exception):
    """Token Empty exception"""

    def __init__(self, value):
        self.value = value
        logger.error(self.value)
        if settings.DEBUG:
            print(self.value)


def gen_hash(size):
    """Generate some hash with given size (length)"""
    alph = "%s%s" % (string.digits, string.ascii_letters)
    h = "".join([random.choice(alph) for i in range(0, size)])
    return h


class UserOption(models.Model):
    """Table for keep user options"""

    user = models.ForeignKey("EmailUser", on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=250)

    def __str__(self):
        return "(%s) %s:%s" % (self.user, self.name, self.value[:10])

class LoginAttempt(models.Model):
    user = models.ForeignKey("EmailUser", on_delete=models.CASCADE)
    added = models.DateTimeField(auto_now_add=True)
    # save user's IP address
    # ip = models.CharField(max_length=50, null=True, blank=True)
    
    def __str__(self):
        return f"{self.user}"
    
class BaseUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    added = models.DateTimeField(auto_now_add=True)
    last_auth = models.DateTimeField(auto_now=True)
    auth_token = models.CharField(max_length=150, default="", null=True, blank=True)
    token_generated = models.DateTimeField(null=True, blank=True)
    is_authenticated = models.BooleanField(default=False)
    int_token = models.CharField(max_length=10, null=True, blank=True)
    auth_token_len = 20
    int_token_len = 6
    token_timelife = timedelta(days=1)
    int_token_timelife = timedelta(minutes=3)

    class Meta:
        abstract = True

    def do_auth(self, token):
        """Check if token is valid
        accepts url (hash) token and int token
        0: int token
        1: str/hash token
        """
        if settings.USE_TZ:
            tz = pytz.timezone(settings.TIME_ZONE)
            last_hour = datetime.now(tz) - timedelta(hours=1)
        else:
            last_hour = datetime.now() - timedelta(hours=1)
                
        la, c = LoginAttempt.objects.get_or_create(user=self)
        if LoginAttempt.objects.filter(added__lt=last_hour).count() > 10:
            raise TokenException("Too many login attempts, please try again later")
        
        try:
            token = str(token)
        except ValueError:
            raise TokenException("Token should contain only acsi characters")

        token_type = 1  # str/hash token
        if len(token) == self.int_token_len:
            token_type = 0  # int_token

        if token_type == 1:
            if len(token) < self.auth_token_len:
                raise TokenException(
                    (
                        f"Token length should be at least {self.auth_token_len} characters: {token} ({len(token)})"
                    )
                )

            if len(self.auth_token) < self.auth_token_len:
                raise TokenException(
                    ("Token is invalid. Create new token with generate_token()")
                )

        if self.token_generated is None:
            raise TokenException(
                (
                    "token_generated is invalid. Create new "
                    "token with generate_token()"
                )
            )
        if settings.USE_TZ:
            tz = pytz.timezone(settings.TIME_ZONE)
            if datetime.now(tz) - self.token_generated > self.token_timelife:
                raise TokenException(
                    ("Token is outdated Create new token with generate_token()")
                )
        else:
            if datetime.now() - self.token_generated > self.token_timelife:
                raise TokenException(
                    ("Token is outdated Create new token with generate_token()")
                )

        authenticated = False
        if token_type == 1 and self.auth_token == token:
            authenticated = True
            # self.auth_token = ""
        elif token_type == 0 and self.int_token == token:
            authenticated = True
        if authenticated:
            self.is_authenticated = True
            self.last_auth = datetime.now()
            self.save()
        return authenticated

    def generate_token(self, new=False):
        """Create new token and store it in database"""
        if settings.USE_TZ:
            tz = pytz.timezone(settings.TIME_ZONE)
            self.token_generated = datetime.now(tz)
        else:
            self.token_generated = datetime.now()

        date_str = self.token_generated.strftime("%m/%d/%Y")
        hash_string = "%s%s%s" % (self.pk, settings.SECRET_KEY, date_str)

        # self.auth_token = gen_hash(self.auth_token_len + random.randint(2, 20))
        self.auth_token = hashlib.sha256(hash_string.encode("utf8")).hexdigest()
        self.int_token = "".join(choice(string.digits) for i in range(0, 6))
        self.save()
        if new:
            return(self.auth_token, self.int_token)
        else:
            return self.auth_token

    def get_option(self, name):
        """Get user option"""
        try:
            uopt = UserOption.objects.get(user=self, name=name)
        except UserOption.DoesNotExist:
            return False
        return uopt.value

    def set_option(self, name, value):
        """Set user option"""
        UserOption.objects.get_or_create(user=self, name=name, value=value)
        return None


class EmailUser(BaseUser):
    """EmailUser model,
    we keep here tokens"""

    def __unicode__(self):
        return self.user.email


class TelegramUser(BaseUser):
    """TelegramUser model,
    we keep here tokens"""

    tg_user_id = models.BigIntegerField(unique=True)
    tg_username = models.CharField(max_length=250, null=True, blank=True)

    def __unicode__(self):
        return str(self.tg_user_id)


class EmailUserForm(forms.Form):
    """EmailUser form (for login/registration"""

    email = forms.EmailField()

    def clean_email(self):
        """All emails should be lower case"""
        super(EmailUserForm, self).clean()
        data = self.cleaned_data["email"].lower()
        # check for common errors
        bad_domains = [
            ".ry",
            "gmail.ru",
            "google.ru",
            "gmaii.com",
            "gmal.com",
            "yndex.ru",
            ".tu",
            "mall.com",
            "yuandex.com",
            "yuandex.ru",
            "yadex.ru",
            "gimail.com",
            "gmaik.com",
        ]
        for bd in bad_domains:
            # print(bd)
            if data.endswith(bd):
                raise forms.ValidationError(
                    "Looks like email is invalid, please try another one"
                )
        return data


@receiver(post_save, sender=User)
def create_email_user(sender, **kw):
    """Here we create emailuser each time new user added"""
    user = kw["instance"]
    if kw["created"]:
        euser = EmailUser(user=user)
        euser.save()


def manual_create_email_user(user):
    """Helper for creating meailuser manually"""
    euser = EmailUser(user=user)
    euser.save()


def manual_create_telegram_user(user, tg_user):
    """Helper for creating telegram user manually"""
    tuser = TelegramUser(user=user, tg_user_id=tg_user.id, tg_username=tg_user.username)
    tuser.save()
